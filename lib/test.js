import fs from 'fs'
import path from 'path'

export function getData(){
  const file = path.join(process.cwd(), 'data/sampleFile');
  const content = fs.readFileSync(file, 'utf-8');

  return {
    content
  }
}