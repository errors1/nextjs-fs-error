import {getData} from '../lib/test';

// import fs from 'fs';

export default function Container({data}){
  return (
    <>
    {data}
    </>
  )

}

export async function getStaticProps(){
  const {content} = getData();

  return {
    props: {
      data: content
    }
  }
}